<?php

$fp=fopen("prime_in_textfile.txt","w+");
$start=450;
$end=1000;
$count=1;
for($num=$start;$num<=$end;$num++)
{
    if($num==1)
        continue;
    $is_prime=true;
    for($i=2;$i<=ceil($num/2);$i++)
    {
        if(($num%$i)==0)
        {
            $is_prime=false;
            break;
        }

    }

    if($is_prime)
    {
        fwrite($fp,"$num ");
        if($count%10==0) {
            fwrite($fp, "\n");
            $count=0;
        }

        $count++;

    }

}


fclose($fp);

?>
